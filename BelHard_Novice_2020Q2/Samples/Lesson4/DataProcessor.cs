﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace BelHard_Novice_2020Q2.Samples.Lesson4
{
    public class DataProcessor
    {
        private string _name;

        private string _surname;

        private bool _isInitialized;

        private FileInfo _fileInfo;

        public DataProcessor(string path, string name, string surname)
        {
            _fileInfo = new FileInfo(path);
            _name = name;
            _surname = surname;
        }

        public bool ReadData()
        {
            _name = ReadData("name");
            _surname = ReadData("surname");

            _isInitialized = _name != null && _surname != null;
            return _isInitialized;
        }

        public void WriteData()
        {
            var outputString = $"Person name: {_name} {_surname}";

            using (var stringWriter = new StreamWriter(_fileInfo.OpenWrite()))
            {
                stringWriter.WriteLine(outputString);
            }
        }

        private string ReadData(string template)
        {
            using (var reader = new StreamReader(_fileInfo.OpenRead()))
            {
                return reader.ReadLine();
            }
        }
    }
}