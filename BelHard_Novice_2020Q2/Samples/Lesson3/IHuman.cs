﻿namespace BelHard_Novice_2020Q2.Samples.Lesson3
{
    public interface IHuman
    {
        string Name { get; }

        public void Speak(string text);
    }
}