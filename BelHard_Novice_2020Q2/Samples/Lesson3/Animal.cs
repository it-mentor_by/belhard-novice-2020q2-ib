﻿namespace BelHard_Novice_2020Q2.Samples.Lesson3
{
    public abstract class Animal
    {
        public Animal(int numberOfLegs)
        {
            NumberOfLegs = numberOfLegs;
        }

        public int Age { get; private set; }

        public int NumberOfLegs { get; private set; }

        public virtual void Walk()
        {
            // do some steps
        }
    }
}