﻿using System;

namespace BelHard_Novice_2020Q2.Samples.Lesson3.Tasks
{
    public class NoIndexSender : IDataSender, IDataLogger
    {
        public void Send(int[] incommingData)
        {
            foreach (var item in incommingData)
            {
                Console.WriteLine($"Value - {item}");
            }
        }

        public void LogData()
        {
            throw new NotImplementedException();
        }
    }
}