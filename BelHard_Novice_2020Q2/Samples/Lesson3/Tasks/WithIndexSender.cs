﻿using System;

namespace BelHard_Novice_2020Q2.Samples.Lesson3.Tasks
{
    public class WithIndexSender : IDataSender
    {
        public void Send(int[] incommingData)
        {
            for (var i = 0; i < incommingData.Length; i++)
            {
                Console.WriteLine($"Index - {i}, value - {incommingData[i]}");
            }
        }
    }
}