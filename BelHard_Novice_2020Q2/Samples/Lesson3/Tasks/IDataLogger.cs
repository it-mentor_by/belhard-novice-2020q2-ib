﻿namespace BelHard_Novice_2020Q2.Samples.Lesson3.Tasks
{
    public interface IDataLogger
    {
        void LogData();
    }
}