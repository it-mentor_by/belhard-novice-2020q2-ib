﻿using System;

namespace BelHard_Novice_2020Q2.Samples.Lesson3
{
    public class Human : Animal, IHuman
    {
        private const int _defaultNumberOfLegs = 2;

        public Human() : base(_defaultNumberOfLegs)
        {
            
        }

        public string Name { get; private set; }

        public override void Walk()
        {
            base.Walk();
        }

        public void Speak(string text)
        {
            throw new NotImplementedException();
        }
    }
}