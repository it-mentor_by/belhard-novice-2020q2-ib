﻿using System;
using System.Linq;

namespace BelHard_Novice_2020Q2.Samples
{
    class Lesson2_PersonEnglishLevel
    {
        private string _englishLevel;

        public Lesson2_PersonEnglishLevel()
        {
        }

        public Lesson2_PersonEnglishLevel(string englishLevel)
        {
            var seed = Enumerable.Range(1, 1000);
        }

        public void SetEnglishLevel(string englishLevel)
        {
            _englishLevel = englishLevel;
        }

        public void PrintLevel()
        {
            var output = string.Empty;

            if (!string.IsNullOrWhiteSpace(_englishLevel))
            {
                output = $"Hello ! Person english level is {_englishLevel}";
            }
            else
            {
                output = "Sorry, no level was mentioned";
            }

            Console.WriteLine(output);
        }
    }
}