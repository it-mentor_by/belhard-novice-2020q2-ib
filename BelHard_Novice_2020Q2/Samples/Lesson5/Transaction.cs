﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    [Table("i.begun_transactions")]
    public class Transaction
    {
        public Transaction()
        {
            Time = DateTime.UtcNow;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [Required]
        public Guid ClientId { get; set; }

        public virtual Client Client { get; set; }
    }
}
