﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    public static class PinGenerator
    {
        private static Random Rnd;

        private static MD5 Hasher;

        private const int PinStart = 1;

        private const int PinEnd = 9999;

        private const string OutputFormat = "D4";

        static PinGenerator()
        {
            Rnd = new Random();
            Hasher = MD5.Create();
        }

        public static string GeneratePin()
        {
            return Rnd.Next(PinStart, PinEnd).ToString(OutputFormat);
        }

        public static string GeneratePinHash(string pin)
        {
            return string.Concat(Hasher.ComputeHash(Encoding.UTF8.GetBytes(pin)).Select(x => x.ToString("X2")));
        }
    }
}
