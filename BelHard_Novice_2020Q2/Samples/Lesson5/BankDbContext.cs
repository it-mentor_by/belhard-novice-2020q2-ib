﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    public class BankDbContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public BankDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Client>()
                .HasMany(x => x.Transactions)
                .WithOne(x => x.Client)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
