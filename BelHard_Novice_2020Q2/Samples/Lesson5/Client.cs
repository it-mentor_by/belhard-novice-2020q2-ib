﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    [Table("i.begun_clients")]
    public class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Pin { get; set; }

        [Required]
        public ClientScoringRate ScoringRate { get; set; }

        public virtual List<Transaction> Transactions { get; set; }
     }
}