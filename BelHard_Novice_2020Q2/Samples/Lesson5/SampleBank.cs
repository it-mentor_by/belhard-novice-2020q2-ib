﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    public class SampleBank : Bank, IBank
    {
        private const decimal LowRate = 10;

        private const int LowRateAgeLimit = 28;

        private const decimal AverageRate = 20;

        private const int AverageRateAgeLimit = 45;

        private const decimal HighRate = 30;

        public SampleBank(BankDbContext context) : base(context)
        {
        }

        public async Task<(Guid id, string pin)> CreateClientAsync(string name, int age)
        {
            var clientRate = base.EvaluateScoringRate(age, LowRateAgeLimit, AverageRateAgeLimit);
            return await base.CreateClientAsync(name, age, clientRate);
        }

        public async Task<List<Client>> GetClientsAsync()
        {
            return await base.GetClientsAsync(default);
        }

        public async Task<decimal> BalanceAsync(Guid id)
        {
            return await base.GetBalanceAsync(id);
        }

        public async Task WithdrawAsync(Guid id, decimal amount)
        {
            var client = (await base.GetClientsAsync(id)).SingleOrDefault();
            var balance = await base.GetBalanceAsync(client.Id);

            var delta = balance - amount;
            var finalAmount = amount; // avoiding editing argument
            if (delta < 0)
            {
                var feeRate = CalculateRate(client.ScoringRate, LowRate, AverageRate, HighRate);
                delta = Math.Abs(delta);
                finalAmount += delta * feeRate - delta;
            }

            await base.WithdrawAsync(client.Id, finalAmount);
        }

        public async Task CashInAsync(Guid id, decimal amount)
        {
            var client = (await base.GetClientsAsync(id)).SingleOrDefault();

            await base.CashInAsync(client.Id, amount);
        }
    }
}
