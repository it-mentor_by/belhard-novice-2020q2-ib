﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    public abstract class Bank
    {
        private readonly BankDbContext _context;

        protected Bank(BankDbContext context)
        {
            _context = context;
        }

        protected async Task<(Guid id, string pin)> CreateClientAsync(string name, int age, ClientScoringRate rate)
        {
            var pin = PinGenerator.GeneratePin();

            var newClient = await _context.Clients.AddAsync(new Client
            {
                Name = name,
                Age = age,
                ScoringRate = rate,
                Pin = PinGenerator.GeneratePinHash(pin)
            });

            await _context.SaveChangesAsync();

            return (newClient.Entity.Id, pin);
        }

        protected async Task<List<Client>> GetClientsAsync(Guid? id)
        {
            var query = _context.Clients.AsQueryable();

            if (id.HasValue)
            {
                query = query.Where(x => x.Id == id.Value);
            }

            return await query.ToListAsync();
        }

        protected async Task WithdrawAsync(Guid id, decimal amount)
        {
            // TODO send sms about withdraw
            await AddTransactionAsync(id, amount);
        }

        protected async Task CashInAsync(Guid id, decimal amount)
        {
            // TODO send sms about top up
            await AddTransactionAsync(id, amount);
        }

        protected decimal CalculateRate(ClientScoringRate rate, decimal lowRate, decimal averageRate, decimal highRate)
        {
            var taxRate = default(decimal);

            switch (rate)
            {
                case ClientScoringRate.Low:
                    taxRate = highRate;
                    break;
                case ClientScoringRate.Average:
                    taxRate = averageRate;
                    break;
                case ClientScoringRate.High:
                    taxRate = lowRate;
                    break;
            }

            return taxRate / 100 + 1;
        }

        protected ClientScoringRate EvaluateScoringRate(int clientAge, int lowAgeLimit, int averageAgeLimit)
        {
            var rate = ClientScoringRate.Low;

            if (clientAge <= lowAgeLimit)
            {
                rate = ClientScoringRate.Low;
            }
            else if (clientAge <= averageAgeLimit)
            {
                rate = ClientScoringRate.Average;
            }
            else
            {
                rate = ClientScoringRate.High;
            }

            return rate;
        }

        private async Task AddTransactionAsync(Guid id, decimal amount)
        {
            var client = await _context.Clients
                .Include(x => x.Transactions)
                .SingleOrDefaultAsync();
            client.Transactions.Add(new Transaction
            {
                Amount = amount
            });
            _context.Update(client).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        protected async Task<decimal> GetBalanceAsync(Guid id)
        {
            var client = await _context.Clients
                .Include(x => x.Transactions)
                .SingleOrDefaultAsync(x => x.Id == id);

            return client.Transactions
                .Select(x => x.Amount)
                .Sum();
        }
    }
}
