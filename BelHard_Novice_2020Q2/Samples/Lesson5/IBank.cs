﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BelHard_Novice_2020Q2.Samples.Lesson5
{
    public interface IBank
    {
        Task<(Guid id, string pin)> CreateClientAsync(string name, int age);

        Task<List<Client>> GetClientsAsync();

        Task<decimal> BalanceAsync(Guid id);

        Task WithdrawAsync(Guid id, decimal amount);

        Task CashInAsync(Guid id, decimal amount);
    }
}