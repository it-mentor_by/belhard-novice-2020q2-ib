﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson12
{
    [Table("i.begun_skud_timeRecord")]
    public class TimeRecord
    {
        [Key]
        public Guid EmployeeId {get;set;}

        [Required]
        public DateTime Time { get; set; }

        public virtual Employee Employee { get; set; }

        public TimeRecord()
        {
            Time = DateTime.UtcNow;
        }
    }
}
