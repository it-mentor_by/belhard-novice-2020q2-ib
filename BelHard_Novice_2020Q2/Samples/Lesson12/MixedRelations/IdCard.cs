﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson12
{
    [Table("i.begun_skud_idCard")]
    public class IdCard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid EmployeeId { get; set; }

        public virtual Employee Employee {get;set;}
    }
}
