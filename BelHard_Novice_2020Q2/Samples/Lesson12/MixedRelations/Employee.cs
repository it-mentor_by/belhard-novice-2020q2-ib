﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson12
{
    [Table("i.begun_skud_employee")]
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public virtual IdCard Card { get; set; }

        public virtual List<TimeRecord> Records { get; set; }

        public Employee()
        {
            
        }
    }
}
