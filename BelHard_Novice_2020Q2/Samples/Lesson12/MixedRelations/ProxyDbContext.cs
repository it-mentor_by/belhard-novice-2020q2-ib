﻿//using System;
//using Microsoft.EntityFrameworkCore;

//namespace BelHard_Novice_2020Q2.Samples.Lesson12
//{
//    public class ProxyDbContext : DbContext
//    {
//        private const string ConnectionString =
//            "Server=tcp:belhardtest.database.windows.net,1433;Initial Catalog=test;Persist Security Info=False;User ID=belhard;Password=8810270148469X_x;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

//        public DbSet<Company> Companies { get; set; }

//        public DbSet<Employee> Employees { get; set; }

//        public DbSet<IdCard> Cards { get; set; }

//        public DbSet<TimeRecord> Records { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder options)
//        {
//            options.UseSqlServer(ConnectionString,
//                x => x.MigrationsHistoryTable("i.begun_skud_migrationsHistory", "i.begun_skud"));
//        }

//        protected override void OnModelCreating(ModelBuilder builder)
//        {
//            builder.Entity<Company>()
//                .HasMany(x => x.Employees)
//                .WithOne(x => x.Company)
//                .HasForeignKey(x => x.CompanyId);

//            builder.Entity<Employee>()
//                .HasOne(x => x.Card)
//                .WithOne(x => x.Employee);

//            builder.Entity<Employee>()
//                .HasMany(x => x.Records)
//                .WithOne();
//        }

//    }
//}
