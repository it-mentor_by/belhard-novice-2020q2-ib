﻿//using System;
//using Microsoft.EntityFrameworkCore;

//namespace BelHard_Novice_2020Q2.Samples.Lesson12.ManyToMany
//{
//    public class KolobikeDbContext : DbContext
//    {
//        private const string ConnectionString =
//            "Server=tcp:belhardtest.database.windows.net,1433;Initial Catalog=test;Persist Security Info=False;User ID=belhard;Password=8810270148469X_x;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

//        protected override void OnConfiguring(DbContextOptionsBuilder options)
//        {
//            options.UseSqlServer(ConnectionString,
//                x => x.MigrationsHistoryTable("i.begun_migrationsHistory_kolobike", "i.begun_kolobike"));

//        }

//        protected override void OnModelCreating(ModelBuilder builder)
//        {
//            //builder.Entity<RiderBycicleRelations>()
//            //.HasKey(x => new[] { x.BycicleId, x.RiderId });

//            builder.Entity<Bycicle>()
//                .HasMany(x => x.Relations)
//                .WithOne(x => x.Bycicle);

//            builder.Entity<Rider>()
//                .HasMany(x => x.Relations)
//                .WithOne(x => x.Rider);

//        }
//    }
//}
