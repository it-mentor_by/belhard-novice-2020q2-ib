﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson12.ManyToMany
{
    [Table("i.begiun_kolobike_riderbycicles")]
    public class RiderBycicleRelations
    {
        [Key]
        public Guid RiderId { get; set; }

        [Key]
        public Guid BycicleId { get; set; }

        public virtual Rider Rider { get; set; }

        public virtual Bycicle Bycicle { get; set; }
    }
}
