﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelHard_Novice_2020Q2.Samples.Lesson12.ManyToMany
{
    [Table("i.begiun_kolobike_bycicle")]
    public class Bycicle
    {
        [Key]
        public Guid Id { get; set; }

        public virtual List<RiderBycicleRelations> Relations { get; set; }
    }
}
