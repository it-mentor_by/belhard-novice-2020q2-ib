﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BelHard_Novice_2020Q2.Migrations
{
    public partial class added_pin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "i.begun_clients",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Pin = table.Column<string>(nullable: false),
                    ScoringRate = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_i.begun_clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "i.begun_transactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_i.begun_transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_i.begun_transactions_i.begun_clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "i.begun_clients",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_i.begun_transactions_ClientId",
                table: "i.begun_transactions",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "i.begun_transactions");

            migrationBuilder.DropTable(
                name: "i.begun_clients");
        }
    }
}
