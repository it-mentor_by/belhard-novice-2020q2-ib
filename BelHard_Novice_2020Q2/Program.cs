﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BelHard_Novice_2020Q2.Samples.Lesson3.Tasks;
using BelHard_Novice_2020Q2.Samples.Lesson4;
using BelHard_Novice_2020Q2.Samples.Lesson5;

namespace BelHard_Novice_2020Q2
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // TODO do not touch, please )
            await ExecuteAsync();
        }

        private static async Task ExecuteAsync()
        {
            //// TODO 1) compute SHA1 of key value
            //// TODO 2) PBDKF2 of passport:identifier with salt as SHA1 of key value

            //var keyNumber = new Random()
            //    .Next(int.MinValue, int.MaxValue)
            //    .ToString();
            //var keyBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(keyNumber));
            //var keyBase64Bytes = Encoding.UTF8.GetBytes(keyBase64);

            //var hasher = SHA1.Create();
            //var salt = hasher.ComputeHash(keyBase64Bytes);

            //var data = "MP3935778:7840065A001PB3";
            //var dataBytes = Encoding.UTF8.GetBytes(data);

            //var provider = new RNGCryptoServiceProvider();
            //provider.GetBytes(salt);
            //var pbkdf2 = new Rfc2898DeriveBytes(dataBytes, salt, 1000);
            //var result = pbkdf2.GetBytes(64);

            //Console.WriteLine(Convert.ToBase64String(result));
            //Console.ReadKey();
        }
    }
}