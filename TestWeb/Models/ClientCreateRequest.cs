﻿using System.ComponentModel.DataAnnotations;

namespace TestWeb.Models
{
    public class ClientCreateRequest
    {
        [Required]
        [MinLength(3)]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required]
        [Range(18, 100)]
        public int Age { get; set; }
    }
}
