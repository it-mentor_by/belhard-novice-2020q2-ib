﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Models
{
    public class ClientCreateResponse
    {
        public Guid Id { get; set; }

        public string Pin { get; set; }
    }
}
