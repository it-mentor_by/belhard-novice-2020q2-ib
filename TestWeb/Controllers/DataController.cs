﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Threading.Tasks;
using BelHard_Novice_2020Q2.Samples.Lesson5;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestWeb.Models;

namespace TestWeb.Controllers
{
    [ApiController]
    [Route("/clients")]
    public class DataController : ControllerBase
    {
        private readonly IBank _service;

        public DataController(IBank service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateClientAsync(ClientCreateRequest request)
        {
            var (id, pin) = await _service.CreateClientAsync(request.Name, request.Age);
            return Ok(new ClientCreateResponse
            {
                Id = id,
                Pin = pin
            });
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetClientsAsync()
        {
            var clients = await _service.GetClientsAsync();
            return Ok(clients);
        }

        [HttpGet]
        [Route("{id}/account")]
        public async Task<IActionResult> GetBalanaceAsync([FromRoute]Guid id)
        {
            var balance = await _service.BalanceAsync(id);
            return Ok(balance);
        }

        [HttpPost]
        [Route("{id}/account")]
        public async Task<IActionResult> ChangeAccountAsync([FromRoute]Guid id, [FromQuery(Name = "amount")] decimal amount)
        {
            if (amount < default(decimal))
            {
                await _service.WithdrawAsync(id, amount);
            }
            else if(amount > default(decimal))
            {
                await _service.CashInAsync(id, amount);
            }

            return Ok();
        }

        //[HttpGet]
        //[Route("{id}")]
        //public IActionResult Get([FromRoute]Guid id)
        //{
        //    //return Ok(_service.Balance);
        //    //_logger.LogInformation("Hello !");
        //    //var rng = new Random();
        //    //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    //{
        //    //    Date = DateTime.Now.AddDays(index),
        //    //    TemperatureC = rng.Next(-20, 55),
        //    //    Summary = Summaries[rng.Next(Summaries.Length)]
        //    //})
        //    //.ToArray();
        //}

        //[HttpGet]
        //[Route("{id}/transactions")]
        //public IActionResult GetClientTransactions([FromRoute] Guid id)
        //{
        //    //return Ok(_service.Balance);
        //    //_logger.LogInformation("Hello !");
        //    //var rng = new Random();
        //    //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    //{
        //    //    Date = DateTime.Now.AddDays(index),
        //    //    TemperatureC = rng.Next(-20, 55),
        //    //    Summary = Summaries[rng.Next(Summaries.Length)]
        //    //})
        //    //.ToArray();
        //}

        //[HttpPost]
        //public IActionResult Post([FromBody]string name)
        //{
        //    return Ok(name.ToUpper());
        //}
    }
}
